/*Copyright (c) 2016-2017 hotmail.com All Rights Reserved.
 This software is the confidential and proprietary information of hotmail.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with hotmail.com*/
package in.aayush.locationsender.locationservice;

import javax.servlet.http.HttpServletRequest;

import com.wavemaker.tools.apidocs.tools.core.model.Response;
import in.aayush.locationsender.common.LocationCache;
import in.aayush.locationsender.locationservice.model.Device;
import in.aayush.locationsender.locationservice.model.LocationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;
import org.springframework.web.client.ResourceAccessException;

import java.util.*;

//import in.aayush.locationsender.locationservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class LocationService {

    private static final Logger logger = LoggerFactory.getLogger(LocationService.class);

    @Autowired
    private LocationCache locationCache;

    public String postLocationToCache(LocationData locationData){
        if(!locationData.checkIntegrity()){
            throw new ResourceAccessException("Bad Request");
        }
        locationCache.storeLocationDataInCache(locationData);
        return "Success";
    }

    public LocationData getLocationFromCache(String deviceId){
        return locationCache.getLocationDataforDeviceId(deviceId);
    }

    public Set<Device> getAvailableDeviceIds(){
        return locationCache.getAvailableDevices();
    }

}
