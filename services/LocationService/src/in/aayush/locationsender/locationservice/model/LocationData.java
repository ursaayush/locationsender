/*Copyright (c) 2016-2017 hotmail.com All Rights Reserved.
 This software is the confidential and proprietary information of hotmail.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with hotmail.com*/
package in.aayush.locationsender.locationservice.model;

/**
 * Created by ajai20 on 18/10/16.
 */
public class LocationData {

    private String deviceId;
    private Double latitude;
    private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean checkIntegrity(){
        if(this.deviceId == null || this.latitude == null || this.longitude == null){
            return false;
        }
        return true;
    }
}
