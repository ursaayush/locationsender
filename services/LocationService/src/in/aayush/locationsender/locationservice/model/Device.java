/*Copyright (c) 2016-2017 hotmail.com All Rights Reserved.
 This software is the confidential and proprietary information of hotmail.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with hotmail.com*/
package in.aayush.locationsender.locationservice.model;

/**
 * Created by ajai20 on 18/10/16.
 */
public class Device {

    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
