var _WM_APP_PROPERTIES = {
  "activeTheme" : "mobile",
  "defaultLanguage" : "en",
  "displayName" : "LocationSender",
  "homePage" : "Main",
  "name" : "LocationSender",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};