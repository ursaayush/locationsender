package in.aayush.locationsender.common;

import com.wavemaker.studio.common.ResourceNotFoundException;
import com.wavemaker.studio.prefab.util.Utils;
import in.aayush.locationsender.locationservice.LocationService;
import in.aayush.locationsender.locationservice.model.Device;
import in.aayush.locationsender.locationservice.model.LocationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ajai20 on 18/10/16.
 */
@Component
public class LocationCache {

    private static final Logger logger = LoggerFactory.getLogger(LocationCache.class);

    final ConcurrentHashMap<String,LocationData> cache = new ConcurrentHashMap<>();

    public void storeLocationDataInCache(LocationData locationData){
        if(locationData == null){
            throw new ResourceAccessException("deviceId and locationData should not be null");
        }
        cache.put(locationData.getDeviceId(),locationData);

    }

    public LocationData getLocationDataforDeviceId(String deviceId) {
        if(deviceId == null){
            throw new ResourceAccessException("deviceId should not be null");
        }
        LocationData ld = cache.get(deviceId);
        if(ld == null){
            throw new ResourceNotFoundException("No Location for this device in cache");
        }
        return ld;
    }

    public Set<Device> getAvailableDevices(){
        Set<Device> devices = new HashSet<>();
        for (String s : cache.keySet()) {
            Device d = new Device();
            d.setDeviceId(s);
            devices.add(d);
        }
        return devices;
    }

}
